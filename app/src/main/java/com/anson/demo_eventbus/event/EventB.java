package com.anson.demo_eventbus.event;

/**
 * Created by Anson on 2015/8/23.
 */
public class EventB {
    private String name;
    private int number;

    public EventB(String name,int number){
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }
}
