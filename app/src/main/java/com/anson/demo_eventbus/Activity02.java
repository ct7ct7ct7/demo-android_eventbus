package com.anson.demo_eventbus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.anson.demo_eventbus.event.EventA;
import com.anson.demo_eventbus.event.EventB;

import de.greenrobot.event.EventBus;

public class Activity02 extends AppCompatActivity implements View.OnClickListener{

    private Button mButton;
    private Button mButton2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_02);

        Button buttonA = (Button)findViewById(R.id.buttonA);
        Button buttonB = (Button)findViewById(R.id.buttonB);

        buttonA.setOnClickListener(this);
        buttonB.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonA:
                EventBus.getDefault().post(new EventA());
                break;
            case R.id.buttonB:
                EventB eventB = new EventB("Anson",9527);
                EventBus.getDefault().post(eventB);
                break;
        }
    }
}
