package com.anson.demo_eventbus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.anson.demo_eventbus.event.EventA;
import com.anson.demo_eventbus.event.EventB;
import com.anson.demo_eventbus.event.EventC;

import de.greenrobot.event.EventBus;

public class Activity01 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_01);

        Button button = (Button)findViewById(R.id.button);
        Button buttonC = (Button)findViewById(R.id.buttonC);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Activity01.this, Activity02.class));
            }
        });
        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventC());
            }
        });
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    //訂閱事件 A
    public void onEvent(EventA eventA) {
        System.out.println("事件A發生啦");
    };

    //訂閱事件 B
    public void onEvent(EventB eventB) {
        System.out.println("B-Name:" + eventB.getName());
        System.out.println("B-Number:" + eventB.getNumber());
    };

    //訂閱事件 C
    public void onEventBackgroundThread(EventC eventC) {
        //這裡因為是註冊onEventBackgroundThread(背景執行序)
        //所以下面的Toast並不會顯示，會出現RuntimeException
        //如果Code裡面有要更新到畫面，請別註冊onEventBackgroundThread !
        System.out.println("事件C發生啦");
        Toast.makeText(this,"事件C發生啦",Toast.LENGTH_LONG).show();
    };
}
